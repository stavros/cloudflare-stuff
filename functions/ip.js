export const onRequest = (request) => {
   var ip = request.request.headers.get("cf-connecting-ip")
   return new Response(ip);
}
